﻿$server = 'localhost'
$port = '1337'

#Test if the TCP Port on the server is open before applying the settings
#If ((Test-NetConnection -ComputerName $server -Port $port).TcpTestSucceeded) {
    Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyServer -Value "$($server):$($port)"
    Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyEnable -Value 1
#}