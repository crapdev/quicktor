﻿using Knapcode.TorSharp;
using Open.Nat;
using System;
using System.ComponentModel;
using System.IO;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiTor
{
    public partial class Form1 : Form
    {
        private Open.Nat.NatDiscoverer discoverer = new NatDiscoverer();
        private Open.Nat.NatDevice device;
        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.textBox1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBox1.Text += text;
            }
        }
        
        private bool running = false;
        private bool awaitingconnect = false;
        private bool disconnect = false;
        private bool awaitingNewRoute = false;
        private bool captchaChecked = false;
        private TorSharpProxy proxy;
        // User Prefs
        private bool verbose;
        private bool autoreconnect;
        private bool autodisconnect;
        private bool autopowershell;
        private bool upnp;
        private bool allowExit;
        private bool avoidCaptchaNetwork;
        
        private DateTime disconnectAt = DateTime.Now.AddMinutes(30);
        private DateTime nextRouteChange;
        private string IP;
        // User Prefs
        public Form1()
        {
            InitializeComponent(); 
            
        }

        private void connectButton_Click(object sender, EventArgs e)
        {

            nextRouteChange = DateTime.Now.AddMinutes(Convert.ToDouble(maxage.Text.ToString()));
            verbose = settings.GetItemChecked(0);
            autoreconnect = settings.GetItemChecked(1);
            autodisconnect = settings.GetItemChecked(2);
            autopowershell = settings.GetItemChecked(3);
            upnp = settings.GetItemChecked(4);
            allowExit = settings.GetItemChecked(5);
            avoidCaptchaNetwork = settings.GetItemChecked(6);
            for (int i = 0; i < settings.Items.Count; i++)
            {
                SetText(settings.GetItemChecked(i) + "" + settings.GetItemCheckState(i) + "\r\n");
            }
            torProcess.RunWorkerAsync();
            awaitingconnect = true;
            running = true;
            connectButton.Enabled = false;
            disconnectButton.Enabled = true;
            resetButton.Enabled = true;
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            disconnect = true;
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            disconnect = false;
            awaitingconnect = false;
            awaitingNewRoute = false;
            running = false;
            PowerShellUnset();
        }

        private void setPowershell_Click(object sender, EventArgs e)
        {
            PowerShellSet();
        }

        private void unsetPowershell_Click(object sender, EventArgs e)
        {
            PowerShellUnset();
        }      

        private async void torProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            SetText("Starting Services..... \r\n \r\n");
            
            var settings = new TorSharpSettings
            {
                ZippedToolsDirectory = Path.Combine(Path.GetTempPath(), "TorZipped"),
                ExtractedToolsDirectory = Path.Combine(Path.GetTempPath(), "TorExtracted"),
                PrivoxyPort = Convert.ToInt32(proxyport.Text),
                TorSocksPort = Convert.ToInt32(torsocksport.Text),
                TorControlPort = Convert.ToInt32(controlport.Text),
                TorControlPassword = controlPassword.Text
            };
            // download tools
            await new TorSharpToolFetcher(settings, new HttpClient()).FetchAsync();
            // execute
            proxy = new TorSharpProxy(settings);
            var handler = new HttpClientHandler
            {
                Proxy = new WebProxy(new Uri("http://localhost:" + settings.PrivoxyPort))
            };
            var httpClient = new HttpClient(handler);

            

            while (running)
            {                
                if (disconnect)
                {
                    proxy.Stop();
                    running = false;
                    disconnect = false;
                    awaitingNewRoute = false;
                    awaitingconnect = false;
                    SetText("disconnected \r\n");
                    if (autopowershell)
                    {
                        SetText("Unsetting Proxy with Powershell \r\n");
                        PowerShellUnset();
                    }
                }

                if (awaitingNewRoute)
                { 
                    await proxy.GetNewIdentityAsync();
                    awaitingNewRoute = false;
                    captchaChecked = false;
                    SetText("new route established \r\n");
                    IP = await httpClient.GetStringAsync("http://api.ipify.org");
                    SetText("Current IP = " + IP + "\r\n");
                }

                if (awaitingconnect)
                {
                    SetText("connecting \r\n");
                    await proxy.ConfigureAndStartAsync();
                    awaitingconnect = false;
                    SetText("connected \r\n");
                    IP = await httpClient.GetStringAsync("http://api.ipify.org");
                    SetText("Current IP = " + IP + "\r\n");
                    if (autopowershell)
                    {
                        SetText("Setting Proxy with Powershell \r\n");
                        PowerShellSet();
                    }
                    
                    
                    captchaChecked = false;
                }

                if (avoidCaptchaNetwork)
                {
                    if (!captchaChecked)
                    {
                        captchaChecked = CaptchNetworkCheck();
                    }
                }

                if (DateTime.Now > nextRouteChange)
                {
                    SetText("New route request (Automatic) \r\n");
                    await proxy.GetNewIdentityAsync();
                    if (avoidCaptchaNetwork)
                    {
                        captchaChecked = CaptchNetworkCheck();
                    }

                    nextRouteChange = DateTime.Now.AddMinutes(Convert.ToDouble(maxage.Text.ToString()));
                    SetText("New route set \r\n");
                    IP = await httpClient.GetStringAsync("http://api.ipify.org");
                    SetText("Current IP = " + IP + "\r\n");
                }

                if (autodisconnect && DateTime.Now > disconnectAt)
                {
                    proxy.Stop();
                    if (autopowershell)
                    {
                        SetText("Unseting proxy with powershell \r\n");
                        PowerShellUnset();

                    }
                }

                System.Threading.Thread.Sleep(1000);
            }

            proxy.Stop();
            proxy.Dispose();
            SetText("Thread Processor Has Exied. You will need to reconnect. \r\n");
        }

        private bool CaptchNetworkCheck()
        {
            if (!captchaChecked)
            {
                try
                {
                    var handler = new HttpClientHandler
                    {
                        Proxy = new WebProxy(new Uri("http://localhost:" + Convert.ToInt32(proxyport.Text)))
                    };
                    var httpClient = new HttpClient(handler);
                    string check = httpClient.GetStringAsync("https://www.google.com/search?source=hp&ei=J3rIXp_DGqmblwSBi6XoAQ&q=bbc&oq=bbc").GetAwaiter().GetResult();

                    if (check.Contains("captcha"))
                    {
                        SetText("We appear to be connected to a captcha network \r\n");
                        return true;
                    }
                    SetText("No captcha check detected \r\n");
                    return true;
                }
                catch (Exception ex)
                {
                    awaitingNewRoute = true;
                    SetText("Captcha check failed with error, " + ex.Message + ". \r\n");

                    SetText("This IS A Captcha network. Changing hosts. \r\n");
                    System.Threading.Thread.Sleep(15000);
                    return true;
                }
            }
            else
            {
                SetText("Already Checked \r\n");
                return true;
            }
        }
        private void PowerShellSet()
        {
            using (Runspace myRunSpace = RunspaceFactory.CreateRunspace())
            {
                myRunSpace.Open();
                using (PowerShell powershell = PowerShell.Create())
                {
                    RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();

                    Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration);
                    runspace.Open();

                    RunspaceInvoke scriptInvoker = new RunspaceInvoke(runspace);
                    scriptInvoker.Invoke("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted -Force");
                    Pipeline pipeline = runspace.CreatePipeline();

                    //Here's how you add a new script with arguments
                    Command myCommand = new Command(@".\SetProxy.ps1");
                    pipeline.Commands.Add(myCommand);
                    // Execute PowerShell script
                    var results = pipeline.Invoke();
                    foreach (var item in results)
                    {
                        SetText(item.ToString());
                    }
                }
            }
        }
        private void PowerShellUnset()
        {
            using (Runspace myRunSpace = RunspaceFactory.CreateRunspace())
            {
                myRunSpace.Open();
                using (PowerShell powershell = PowerShell.Create())
                {
                    RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();

                    Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration);
                    runspace.Open();

                    RunspaceInvoke scriptInvoker = new RunspaceInvoke(runspace);
                    scriptInvoker.Invoke("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted -Force");
                    Pipeline pipeline = runspace.CreatePipeline();

                    //Here's how you add a new script with arguments
                    Command myCommand = new Command(@".\UnsetProxy.ps1");
                    pipeline.Commands.Add(myCommand);
                    // Execute PowerShell script
                    var results = pipeline.Invoke();
                    foreach (var item in results)
                    {
                        SetText(item.ToString());
                    }
                }
            }
        }
        private void currentaddress_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(IP))
                currentaddress.Text = IP;
        }

        private void settings_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
