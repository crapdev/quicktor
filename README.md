﻿# IF you dont want to build it yourself;
	An installer is available, use the DOWNLOADS Link to the left, and from there extract GuiTor.zip
	And Run setup.exe, this will place the application on your start bar as GuiTor, open and go

# Quick Tor | Gui Tor
	Quick Tor was created in a hurry, its not secure for any thing other than basic browsing.

	The tool was created after pastebin went all homo erectus with there api, taking away what we have paid
	for, so rather than pay for there service i wrote this to allow my trawler continued unimpeeded access to 
	to there archives, you dont need the api or there search anymore, and yes we can even get TEXT.

# Captcha Network Modes
	As we all know, TOR isnt very well liked by many service providers, and as a result, you get them annoying
	captcha cards, well not here, upon connection, we test with google and cloudflare to detect if we are being 
	asked for captcha.

	If a captcha request is found, the circut is dropped and a new one is created, this happens untill captcha passes
	testing

## Side note on captcha.
	Each check takes about 15 seconds, 14seconds of that is sleeping, the underlying tor components do not support network
	switching at faster than once every 15 seconds

	Detection rate of captcha, and automatic network switching has so far been about 99.8% Accurate, accurate enough
	for you to code the fringe cases

# Highly Configurable

	1) Verbose logging, outputs extensive information about internal operations as there running, very handy for the likes of us
	2) Auto Reconnect - Reconnect if the circet unexpecidly fails
	3) Auto Disconnect (30Mins) - Close the proxy after 30mins no matter what 
	4) Auto PowerShell Configuration - Executes power shell scripts (included) to set proxy details on windows 10, and unset them after
	5) Use UPNP Port Manager - UPNP Can be used in some instances to map ports for tor, especially usefull for Virgin Media Customers
	6) Allow Exit Node - Not currently implemented. Open an issue if youd like to see it, i have no need for it right now
	7) Avoid Captcha Network - Youll probably want to use this

All values are false at start

All ports can be configured ( You will need to update the powershell scripts for you usage )

### DOES NOT REQUIRE ADMINISTRATOR PRIVS
### SETTINGS WILL BE LOST ON REBOOT


# Todo

	Configuration Files
	Fix UPNP Support - Screwy with some UK modems
	Add Possibly Exit Node Support

# Quick Start
	1) Install
	2) Open
	3) Tick Autopowershell Box
	4) Tick Avoid Captcha Network Box
	5) Press Connect
	6) Read Output wait for connection
	7) Click CURRENT IP ADDRESS OF THIS PROXY
	8) Verify with google this is your current IP
	9) Disconnect when done
	10) Optional - Click Powershell Unset Proxy (its already called but its nice to be sure)

	Thats it.

# Other uses
	When on public wifi
	Circumvent GEOIP Restrictions
	ByPass ISP Filters (Pirate Proxy etc)
